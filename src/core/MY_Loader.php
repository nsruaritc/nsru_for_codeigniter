<?php
namespace NSRU_CI;

class MY_Loader extends \CI_Loader {

    public function __construct()
    {
        parent::__construct();
    }

    function nsru_view($view, $vars = array(), $return = FALSE) {
        $this->_ci_view_paths = array_merge($this->_ci_view_paths, array(__DIR__ . '/../views/' => TRUE));
        return $this->_ci_load(array(
            '_ci_view' => $view,
            '_ci_vars' => $this->_ci_object_to_array($vars),
            '_ci_return' => $return
        ));
    }

    protected function _ci_object_to_array($object) {
        return is_object($object) ? get_object_vars($object) : $object;
    }

}

?>