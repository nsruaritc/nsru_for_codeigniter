<?php
/**
 * Created by PhpStorm.
 * User: napadon
 * Date: 18/9/2561
 * Time: 1:01 น.
 */

namespace NSRU_CI;


class Installer
{
    public $files = array(
        'core/MY_Loader.php'
    );

    public static function install()
    {
        $installer = new Installer();

        foreach ($installer->files as $file)
        {
            $source = __DIR__ . '/' . $file;
            $dest = APPPATH . $file;

            copy($source, $dest);
        }

    }
}