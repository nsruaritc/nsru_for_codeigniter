<?php
namespace NSRU_CI;

use NSRU\App;

class NSRU_UserController extends \CI_Controller
{
    public $nsru_app_id;
    public $nsru_app_secret;

    private $app;
    private $auth;

    public $is_profile_enable = TRUE;
    public $page_after_signin = "";
    public $page_after_signout = "";
    private $class_name;

    public function __construct()
    {
        parent::__construct();

        $this->load = new MY_Loader();
        $this->load->library('session');
        $this->load->helper('url');

        $this->class_name = get_class($this);

        $this->app = new App($this->nsru_app_id, $this->nsru_app_secret);
        $this->auth = $this->app->createMyAuth();

        if(!$this->auth)
            throw new \Exception("ไม่สามารถสร้าง MyAuth Object ได้. ลองตรวจสอบ nsru_app_id และ nsru_app_secret ว่าถูกต้อง. สอบถามเพิ่มเติมได้ที่กลุ่มงานพัฒนาระบบสารสนเทศ มหาวิทยาลัยราชภัฏนครสวรรค์ หมายเลขโทรศัพท์ภายใน 1521");

        if($this->page_after_signin == "")
        {
            $this->page_after_signin = site_url("$this->class_name/profile");
        }


        if($this->page_after_signout == "")
            $this->page_after_signout = site_url("/");


    }

    public function signin()
    {
        $class_name = get_class($this);
        $postback_url = site_url("$class_name/signin_postback");
        $signin_url = $this->auth->getSigninURL($postback_url);

        redirect($signin_url);
    }

    public function signin_postback()
    {
        $username = $this->input->post("username");
        if($username != "")
        {
            $this->session->set_userdata('username', $username);
        }

        $this->auth->doSigninPostback($this->page_after_signin);
    }

    public function signout()
    {
        $class_name = get_class($this);
        $postback_url = site_url("$class_name/signout_postback");
        $signout_url = $this->auth->getSignoutURL($postback_url);

        redirect($signout_url);
    }

    public function signout_postback()
    {
        $this->session->unset_userdata('username');

        $this->auth->doSignoutPostback($this->page_after_signout);
    }

    public function profile()
    {
        if($this->session->userdata("username") != "")
        {
            if($this->is_profile_enable)
            {
                $data = array(
                    'username' => $this->session->userdata("username"),
                    'signout_url' => site_url("$this->class_name/signout")
                );
                $this->load->nsru_view('profile_page', $data);
            } else {
                echo "Profile page is disabled";
            }
        } else {
            redirect("$this->class_name/signin");
        }

    }

}